---
title: "Week 1"
author: "Chenoa van Nassau"
date: "May 7, 2019"
output: pdf_document
---
### dVolume / dt = a - p * (Volume + a)


# 1.0 Opdracht

## 1

$$\frac{dR}{dt} = -rR + m$$
# y = -ax + b

r = rate of decay
R = de x dus die kan steeds veranderen
m = als het ware de b, aantal nieuwe transcripties

dus r en m moeten geprogrammeerd worden, je moet dan met de parameters gaan spelen.


## 2

https://www.physicsforums.com/threads/multiplying-by-dr-dt-and-integrating-with-respect-to-t.918157/
https://scientificsentence.net/Equations/Mechanics/kinematics/index.php?key=yes&Integer=kinematics
https://www.uv.es/gfl/documentos/bioessays.pdf

Als je de formule in google ging opzoeken krijg je niet specifiek de formule terug, maar verschillende afleidingen ervan die te maken hebben met de natuurkunde en eentje zelfs een beetje met wiskunde. Dus vandaar dat ik voor deze drie heb gekozen, omdat deze het redelijk goed aangeven, maar nogsteeds meer een afleiding ervan of een soortgelijke manier van die formule.

## 3

![drawn model](model.png)

synthese            afbraak
m (+) ----> R ---> r(-)
m = aantal neiuwe transcitpies per seconde
r = de afbraak fractie.


## 4

dit model geeft de verandering van y terug. Dit wordt gedaan in de vorm van een lijst, want de ode functie een lijst verwacht.
\pagebreak

# 2.0 Programmeeropdracht

## a
steady state

## b
increase state

## c
decrease state

```{r fig.cap="plot of mRNA transcripts over time"}

library('deSolve')
parameters <- c(m=125, r=1) # increase b
parameters.1 <- c(m=75, r=1) # decrease c
parameters.2 <- c(m=100, r=1) # steady a

volume <- function(t,y,parms){
  with(as.list(c(parms)),{
    dY <- -r*y + m
    return(list(c(dY)))
  }
  )
}

#initial state
state <- c(Transcripts = 100)
#define time sequence you want to run the model
times <- seq(0, 6,  by = 0.1)
#out to eventually make plots, with different parameters
out  <- ode(times = times, y = state,   parms = parameters, 
            func = volume, method = "euler") # increase
out.1  <- ode(times = times, y = state,   parms = parameters.1, 
              func = volume, method = "euler") # decrease
out.2  <- ode(times = times, y = state,   parms = parameters.2, 
              func = volume, method = "euler") # steady
#plot(out)
plot(out, out.1, out.2, type='l', col=c('green', 'blue', 'red'), 
     main='amount of mRNA transcripts over time'
     , xlab='timepoints per seconde', ylab='number of transcripts', lty='solid')
legend(x=4, y=123, legend = c('steady', 'increasing', 'decreasing'), 
       fill=c('red', 'green', 'blue'))

```

